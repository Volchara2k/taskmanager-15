package ru.renessans.jvschool.volkov.tm.entity;

public enum UserRole {

    ADMIN("Администратор"),

    USER("Пользователь");

    private final String title;

    UserRole(final String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

}