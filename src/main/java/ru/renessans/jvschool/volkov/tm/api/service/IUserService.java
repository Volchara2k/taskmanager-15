package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.entity.UserRole;
import ru.renessans.jvschool.volkov.tm.model.User;

import java.util.Collection;

public interface IUserService {

    User getById(String id);

    User getByLogin(String login);

    Collection<User> getAll();

    User add(String login, String password);

    User add(String login, String password, String firstName);

    User add(String login, String password, UserRole role);

    User editProfileById(String id, String firstName);

    User editProfileById(String id, String firstName, String lastName);

    User updatePasswordById(String id, String newPassword);

    User removeById(String id);

    User removeByLogin(String login);

    User removeByUser(User user);

    Collection<User> initUserTestEntries();

}