package ru.renessans.jvschool.volkov.tm.command;

import ru.renessans.jvschool.volkov.tm.api.service.IServiceLocator;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    protected AbstractCommand() {
    }

    protected AbstractCommand(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getCommand();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract void execute();

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (getCommand() != null && !getCommand().isEmpty())
            result.append("Терминальная команда: ").append(getCommand());
        if (getArgument() != null && !getArgument().isEmpty())
            result.append(", программный аргумент: ").append(getArgument());
        if (getDescription() != null && !getDescription().isEmpty())
            result.append("\n\t - ").append(getDescription());
        return result.toString();
    }

}