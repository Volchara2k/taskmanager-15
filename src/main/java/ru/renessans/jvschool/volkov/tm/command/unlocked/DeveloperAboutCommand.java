package ru.renessans.jvschool.volkov.tm.command.unlocked;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class DeveloperAboutCommand extends AbstractCommand {

    private static final String CMD_ABOUT = "about";

    private static final String ARG_ABOUT = "-a";

    private static final String DESC_ABOUT = "вывод информации о разработчике";

    private static final String NOTIFY_ABOUT = "%s - разработчик; \n%s - почта.";

    private static final String DEVELOPER = "Valery Volkov";

    private static final String DEVELOPER_MAIL = "volkov.valery2013@yandex.ru";

    @Override
    public String getCommand() {
        return CMD_ABOUT;
    }

    @Override
    public String getArgument() {
        return ARG_ABOUT;
    }

    @Override
    public String getDescription() {
        return DESC_ABOUT;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(String.format(NOTIFY_ABOUT, DEVELOPER, DEVELOPER_MAIL));
    }

}