package ru.renessans.jvschool.volkov.tm.command.locked.task;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskUpdateByIdCommand extends AbstractCommand {

    private static final String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";

    private static final String DESC_TASK_UPDATE_BY_ID = "обновить задачу по идентификатору";

    private static final String NOTIFY_TASK_UPDATE_BY_ID =
            "Для обновления задачи по идентификатору введите идентификатор задачи из списка.\n" +
                    "Для обновления задачи введите её заголовок или заголовок с описанием.\n";

    @Override
    public String getCommand() {
        return CMD_TASK_UPDATE_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_UPDATE_BY_ID;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_TASK_UPDATE_BY_ID);
        final String id = ViewUtil.getInstance().getLine();
        final String title = ViewUtil.getInstance().getLine();
        final String description = ViewUtil.getInstance().getLine();
        final Task updatedTask = super.serviceLocator.getTaskService().updateById(userId, id, title, description);
        ViewUtil.getInstance().print(updatedTask);
    }

}