package ru.renessans.jvschool.volkov.tm.command.user;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProfileViewCommand extends AbstractCommand {

    private static final String CMD_VIEW_PROFILE = "view-profile";

    private static final String DESC_VIEW_PROFILE = "обновить пароль пользователя";

    private static final String NOTIFY_VIEW_PROFILE = "Информация о текущем профиле пользователя: \n";

    @Override
    public String getCommand() {
        return CMD_VIEW_PROFILE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_VIEW_PROFILE;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_VIEW_PROFILE);
        final User user = super.serviceLocator.getUserService().getById(userId);
        ViewUtil.getInstance().print(user);
    }

}