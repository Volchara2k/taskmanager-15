package ru.renessans.jvschool.volkov.tm.command.locked.project;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectViewByIndexCommand extends AbstractCommand {

    private static final String CMD_PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    private static final String DESC_PROJECT_VIEW_BY_INDEX = "просмотреть проект по индексу";

    private static final String NOTIFY_PROJECT_VIEW_BY_INDEX_MSG =
            "Для отображения проекта по индексу введите индекс проекта из списка.\n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_VIEW_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_VIEW_BY_INDEX;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_PROJECT_VIEW_BY_INDEX_MSG);
        final Integer index = ViewUtil.getInstance().getInteger() - 1;
        final Project project = super.serviceLocator.getProjectService().getByIndex(userId, index);
        ViewUtil.getInstance().print(project);
    }

}