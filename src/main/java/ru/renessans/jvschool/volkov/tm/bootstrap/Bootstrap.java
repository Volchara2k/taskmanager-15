package ru.renessans.jvschool.volkov.tm.bootstrap;

import ru.renessans.jvschool.volkov.tm.api.repository.IAuthRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.ICrudRepository;
import ru.renessans.jvschool.volkov.tm.api.repository.IUserRepository;
import ru.renessans.jvschool.volkov.tm.api.service.*;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.exception.unknown.UnknownCommandException;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.repository.*;
import ru.renessans.jvschool.volkov.tm.service.*;
import ru.renessans.jvschool.volkov.tm.util.ScannerUtil;
import ru.renessans.jvschool.volkov.tm.util.ValidRuleUtil;

import java.util.Collection;
import java.util.Objects;

public final class Bootstrap implements IServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);


    private final IAuthRepository authRepository = new AuthRepository();

    private final IAuthService authService = new AuthService(userService, authRepository);


    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(this, commandRepository);


    private final ICrudRepository<Task> taskRepository = new TaskRepository();

    private final ICrudService<Task> taskService = new TaskService(taskRepository);


    private final ICrudRepository<Project> projectRepository = new ProjectRepository();

    private final ICrudService<Project> projectService = new ProjectService(projectRepository);

    {
        commandService.initCommands();
        final Collection<User> users = userService.initUserTestEntries();
        taskService.assignTestData(users);
        projectService.assignTestData(users);
    }

    @Override
    public IUserService getUserService() {
        return this.userService;
    }

    @Override
    public IAuthService getAuthService() {
        return this.authService;
    }

    @Override
    public ICrudService<Task> getTaskService() {
        return this.taskService;
    }

    @Override
    public ICrudService<Project> getProjectService() {
        return this.projectService;
    }

    @Override
    public ICommandService getCommandService() {
        return this.commandService;
    }

    public void run(final String... args) {
        final boolean emptyArgs = ValidRuleUtil.isNullOrEmpty(args);
        if (emptyArgs) terminalCommandPrintLoop();
        else argumentPrint(args);
    }

    private void terminalCommandPrintLoop() {
        final String interrupt = "exit";
        String command = "";
        while (!interrupt.equals(command)) {
            command = ScannerUtil.getLine();
            try {
                final AbstractCommand abstractCommand = this.commandService.getTerminalCommand(command);
                if (Objects.isNull(abstractCommand)) throw new UnknownCommandException(command);
                abstractCommand.execute();
            } catch (final Exception e) {
                System.err.print(e.getMessage());
            }
        }
    }

    private void argumentPrint(final String... args) {
        final String arg = args[0];
        try {
            final AbstractCommand abstractCommand =
                    this.commandService.getArgumentCommand(arg);
            if (Objects.isNull(abstractCommand)) throw new UnknownCommandException(arg);
            abstractCommand.execute();
        } catch (final Exception e) {
            System.err.print(e.getMessage());
        }
    }

}