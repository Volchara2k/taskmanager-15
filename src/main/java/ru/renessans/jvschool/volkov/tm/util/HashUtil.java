package ru.renessans.jvschool.volkov.tm.util;

import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyHashLineException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashUtil {

    private static final String SEPARATOR_KEY = "t2ijtnoi23nfog";

    private static final int ITERATOR_KEY = 15423;

    private static volatile HashUtil INSTANCE;

    public static HashUtil getInstance() {
        final HashUtil result = INSTANCE;
        if (result != null) return result;

        synchronized (HashUtil.class) {
            if (INSTANCE == null) INSTANCE = new HashUtil();
            return INSTANCE;
        }
    }

    private HashUtil() {
    }

    public String saltHashLine(final String line) {
        if (ValidRuleUtil.isNullOrEmpty(line)) throw new EmptyHashLineException();
        String hashLine = line;
        for (int i = 0; i < ITERATOR_KEY; i++) {
            hashLine = hashLineMD5(SEPARATOR_KEY + line + SEPARATOR_KEY);
        }
        return hashLine;
    }

    public String hashLineMD5(final String line) {
        if (ValidRuleUtil.isNullOrEmpty(line)) throw new EmptyHashLineException();
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            final byte[] byteArray = messageDigest.digest(line.getBytes(StandardCharsets.UTF_8));
            final StringBuilder stringBuilder = new StringBuilder();
            for (byte symbol : byteArray) {
                stringBuilder.append(Integer.toHexString((symbol & 0xFF) | 0x100), 1, 3);
            }
            return stringBuilder.toString();
        } catch (final NoSuchAlgorithmException e) {
            e.getStackTrace();
        }
        return null;
    }

}