package ru.renessans.jvschool.volkov.tm.entity;

public enum AuthState {

    SUCCESS("Успешно!"),

    APPLICATION_ERROR("Ошибка приложения!"),

    USER_NOT_FOUND("Пользователь не найден!"),

    INVALID_PASSWORD("Некорректный пароль!");

    private final String title;

    AuthState(final String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }

    public boolean isSuccess() {
        return this == SUCCESS;
    }

}