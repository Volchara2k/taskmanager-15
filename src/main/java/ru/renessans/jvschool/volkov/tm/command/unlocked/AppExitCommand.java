package ru.renessans.jvschool.volkov.tm.command.unlocked;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class AppExitCommand extends AbstractCommand {

    private static final String CMD_EXIT = "exit";

    private static final String DESC_EXIT = "закрыть приложение";

    private static final String NOTIFY_EXIT = "Выход из приложения!";

    @Override
    public String getCommand() {
        return CMD_EXIT;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_EXIT;
    }

    @Override
    public void execute() {
        ViewUtil.getInstance().print(NOTIFY_EXIT);
    }

}