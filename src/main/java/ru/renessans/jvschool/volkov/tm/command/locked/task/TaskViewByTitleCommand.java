package ru.renessans.jvschool.volkov.tm.command.locked.task;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Task;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class TaskViewByTitleCommand extends AbstractCommand {

    private static final String CMD_TASK_VIEW_BY_TITLE = "task-view-by-title";

    private static final String DESC_TASK_VIEW_BY_TITLE = "просмотреть задачу по заголовку";

    private static final String NOTIFY_TASK_VIEW_BY_TITLE =
            "Для отображения задачи по заголовку введите заголовок задачи из списка.\n";

    @Override
    public String getCommand() {
        return CMD_TASK_VIEW_BY_TITLE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_TASK_VIEW_BY_TITLE;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_TASK_VIEW_BY_TITLE);
        final String title = ViewUtil.getInstance().getLine();
        final Task task = super.serviceLocator.getTaskService().removeByTitle(userId, title);
        ViewUtil.getInstance().print(task);
    }

}