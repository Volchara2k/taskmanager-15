package ru.renessans.jvschool.volkov.tm.exception.security;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public final class UserAccessFailureException extends AbstractRuntimeException {

    private static final String ACCESS_FAILURE = "Ошибка! Сбой доступа!\n";

    private static final String ACCESS_FAILURE_FORMAT = "Ошибка! %s Сбой доступа!\n";

    public UserAccessFailureException() {
        super(ACCESS_FAILURE);
    }

    public UserAccessFailureException(final String message) {
        super(String.format(ACCESS_FAILURE_FORMAT, message));
    }

}