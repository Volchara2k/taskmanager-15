package ru.renessans.jvschool.volkov.tm.command.user;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.User;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProfileEditCommand extends AbstractCommand {

    private static final String CMD_EDIT_PROFILE = "edit-profile";

    private static final String DESC_EDIT_PROFILE = "изменить данные пользователя";

    private static final String NOTIFY_EDIT_PROFILE =
            "Для обновления данных пользователя введите его имя или имя с фамилией: \n";

    @Override
    public String getCommand() {
        return CMD_EDIT_PROFILE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_EDIT_PROFILE;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_EDIT_PROFILE);
        final String firstName = ViewUtil.getInstance().getLine();
        final User user = this.serviceLocator.getUserService().editProfileById(userId, firstName);
        ViewUtil.getInstance().print(user);
    }

}