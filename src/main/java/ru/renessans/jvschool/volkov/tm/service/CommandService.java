package ru.renessans.jvschool.volkov.tm.service;

import ru.renessans.jvschool.volkov.tm.api.repository.ICommandRepository;
import ru.renessans.jvschool.volkov.tm.api.service.ICommandService;
import ru.renessans.jvschool.volkov.tm.api.service.IServiceLocator;
import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.exception.empty.EmptyCommandException;

import java.util.*;

public final class CommandService implements ICommandService {

    private final IServiceLocator serviceLocator;

    private final ICommandRepository commandRepository;

    public CommandService(
            final IServiceLocator serviceLocator,
            final ICommandRepository commandRepository
    ) {
        this.serviceLocator = serviceLocator;
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> initCommands() {
        final Collection<AbstractCommand> commands = this.commandRepository.allCommand();
        commands.forEach(command -> command.setServiceLocator(this.serviceLocator));
        return commands;
    }

    @Override
    public Collection<AbstractCommand> allCommands() {
        return this.commandRepository.allCommand();
    }

    @Override
    public Collection<AbstractCommand> allTerminalCommands() {
        return this.commandRepository.allTerminalCommands();
    }

    @Override
    public Collection<AbstractCommand> allArgumentCommands() {
        return this.commandRepository.allArgumentCommands();
    }

    @Override
    public AbstractCommand getTerminalCommand(final String command) {
        if (Objects.isNull(command)) throw new EmptyCommandException();
        return this.commandRepository.getTerminalCommand(command);
    }

    @Override
    public AbstractCommand getArgumentCommand(final String command) {
        if (Objects.isNull(command)) throw new EmptyCommandException();
        return this.commandRepository.getArgumentCommand(command);
    }

}