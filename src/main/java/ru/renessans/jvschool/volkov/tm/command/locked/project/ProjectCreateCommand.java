package ru.renessans.jvschool.volkov.tm.command.locked.project;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

public final class ProjectCreateCommand extends AbstractCommand {

    private static final String CMD_PROJECT_CREATE = "project-create";

    private static final String DESC_PROJECT_CREATE = "добавить новый проект";

    private static final String NOTIFY_PROJECT_CREATE =
            "Для создания проекта введите его заголовок или заголовок с описанием.\n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_CREATE;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_CREATE;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_PROJECT_CREATE);
        final String title = ViewUtil.getInstance().getLine();
        final String description = ViewUtil.getInstance().getLine();
        final Project project = super.serviceLocator.getProjectService().add(userId, title, description);
        ViewUtil.getInstance().print(project);
    }

}