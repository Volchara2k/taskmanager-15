package ru.renessans.jvschool.volkov.tm.exception.empty;

import ru.renessans.jvschool.volkov.tm.exception.AbstractRuntimeException;

public class EmptyFirstNameException extends AbstractRuntimeException {

    private static final String EMPTY_FIRST_NAME = "Ошибка! Параметр \"имя\" является пустым или null!\n";

    public EmptyFirstNameException() {
        super(EMPTY_FIRST_NAME);
    }

}