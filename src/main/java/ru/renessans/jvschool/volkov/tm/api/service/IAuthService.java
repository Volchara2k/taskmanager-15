package ru.renessans.jvschool.volkov.tm.api.service;

import ru.renessans.jvschool.volkov.tm.entity.AuthState;
import ru.renessans.jvschool.volkov.tm.entity.UserRole;
import ru.renessans.jvschool.volkov.tm.model.User;

public interface IAuthService {

    String getUserId();

    AuthState signIn(String login, String password);

    User signUp(String login, String password);

    User signUp(String login, String password, String email);

    User signUp(String login, String password, UserRole role);

    boolean logOut();

}