package ru.renessans.jvschool.volkov.tm.command.locked.project;

import ru.renessans.jvschool.volkov.tm.command.AbstractCommand;
import ru.renessans.jvschool.volkov.tm.model.Project;
import ru.renessans.jvschool.volkov.tm.util.ViewUtil;

import java.util.Collection;

public final class ProjectListCommand extends AbstractCommand {

    private static final String CMD_PROJECT_LIST = "project-list";

    private static final String DESC_PROJECT_LIST = "вывод списка проектов";

    private static final String NOTIFY_PROJECT_LIST = "Текущий список проектов: \n";

    @Override
    public String getCommand() {
        return CMD_PROJECT_LIST;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESC_PROJECT_LIST;
    }

    @Override
    public void execute() {
        final String userId = super.serviceLocator.getAuthService().getUserId();
        ViewUtil.getInstance().print(NOTIFY_PROJECT_LIST);
        final Collection<Project> projects = super.serviceLocator.getProjectService().getAll(userId);
        ViewUtil.getInstance().print(projects);
    }

}